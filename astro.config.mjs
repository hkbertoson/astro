import {defineConfig} from 'astro/config';

export default defineConfig({
	outDir: 'public',
	publicDir: 'static',
});
